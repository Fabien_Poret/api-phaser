$(document).ready(function () {
    const config = {
        // // parent: 'phaser-example',
        width: 900,
        height: 600,
        ditype: Phaser.CANVAS,
        splay: 'block',
        marginHorizontal: '0',
        backgroundColor: 'black',
        type: Phaser.auto,
        physics: {
            default: 'arcade',
            arcade: {
                gravity: {
                    y: 300
                },
                debug: false
            }
        },
        scene: {
            preload: preload,
            create: create,
            update: update,
        },
    }

    var game = new Phaser.Game(config);
    let dude;
    let cursors;

    var left;
    var right;
    var up;
    var down;
    var platforms;
    var stars;
    var bombs;

    var score = 0;
    var scoreText;
    var gameOver = false;

    var group;
    var gfx;



    function preload() {
        this.load.spritesheet('dude', 'images/character.png', {
            frameWidth: 100,
            frameHeight: 74
        });
        this.load.image('ground', 'images/ground.png');
        this.load.image('higround', 'images/higround.png');
        this.load.image('star', 'images/coeur.png');
        this.load.image('bomb', 'images/bomb.png');
        this.load.image('fusee', 'images/fusee.png');

        this.load.image('block', 'images/block.png');
        // http://labs.phaser.io/edit.html?src=src/physics/arcade/body%20on%20a%20path.js

    }

    function create() {




        // Créer le physique de la bomb
        bombs = this.physics.add.group();

        // Création d'une étoile 
        // début de la boucle à X
        // StepX tout les espaces des éléments
        stars = this.physics.add.group({
            key: 'star',
            repeat: Phaser.Math.Between(5, 8),
            setXY: {
                x: Phaser.Math.Between(100, 500),
                y: 0,
                stepX: Phaser.Math.Between(50, 100)
            }
        });

        //    Ajout du rebond pour un effet visuel
        stars.children.iterate(function (child) {
            child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));
        });


        block = this.physics.add.group({
            key: 'fusee',
            repeat: Phaser.Math.Between(5, 8),
            setXY: {
                x: Phaser.Math.Between(200, 600),
                y: 0,
                stepX: Phaser.Math.Between(100, 200)
            }
        });

        //    Ajout du rebond pour un effet visuel
        block.children.iterate(function (child) {
            child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));
        });


        platforms = this.physics.add.staticGroup();
        platforms.create(500, 570, 'ground').setScale(1, 1).refreshBody();
        // platforms.create(800, 400, 'higround');
        platforms.create(100, 220, 'higround');
        platforms.create(1200, 220, 'higround');
        platforms.create(650, 300, 'block');

        var block1 = platforms.create(450, 150, 'block');
        var block2 = platforms.create(850, 150, 'block');

        var block3 = platforms.create(450, 400, 'block');
        var block4 = platforms.create(850, 400, 'block');

        var block5 = platforms.create(1450, 400, 'block');

        dude = this.physics.add.sprite(48, 48, 'dude', 2);


        // Création d'une animation
        this.anims.create({
            key: 'up',
            frames: this.anims.generateFrameNumbers('dude', {
                start: 4,
                end: 4
            }),
            frameRate: 10,
            repeat: 1
        });
        this.anims.create({
            key: 'down',
            frames: this.anims.generateFrameNumbers('dude', {
                start: 6,
                end: 6
            }),
            frameRate: 10,
            repeat: 1
        });

        this.anims.create({
            key: 'left',
            frames: this.anims.generateFrameNumbers('dude', {
                start: 16,
                end: 18
            }),
            frameRate: 10,
            repeat: 1
        });
        this.anims.create({
            key: 'right',
            frames: this.anims.generateFrameNumbers('dude', {
                start: 13,
                end: 15
            }),
            frameRate: 10,
            repeat: 1
        });

        // Créer une variable pour récupérer les touches du cursors
        cursors = this.input.keyboard.createCursorKeys();

        // Ajout d'un rebont quand le personnage tombe
        dude.setBounce(0);

        // Empêche le personnage de sortir de l'écran 
        dude.setCollideWorldBounds(true);

        // Ajout la physique des platforms 
        this.physics.add.collider(dude, platforms);
        this.physics.add.collider(bombs, platforms);
        this.physics.add.collider(stars, platforms);
        this.physics.add.collider(block, platforms);

        // Agrandissment de la carte 
        this.physics.world.setBounds(0, 0, 1500, 600);

        // La camera suit le personnage
        this.cameras.main.startFollow(dude);
        this.cameras.main.setBounds(0, 0, 1500, 600);

        // Appel de la fonction collectstar
        this.physics.add.overlap(dude, stars, collectStar, null, this);
        this.physics.add.overlap(dude, block, collectStar, null, this);
        this.physics.add.overlap(dude, bombs, hitBomb, null, this);

        // Ajout du texte
        scoreText = this.add.text(16, 16, 'Score : 0', {
            fontSize: '32px',
            fill: '#000'
        });

        var x = (dude.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);
        var bomb = bombs.create(x, 16, 'bomb');
        bomb.setBounce(1);
        bomb.setCollideWorldBounds(true);
        bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);
        bomb.allowGravity = false

        // Si la variable gameOver est vrai alors arreter le jeu
        if (gameOver) {
            return;
        }

        dude.body.setSize(70, 75, 170, 0);
    }

    function update() {



        if (cursors.up.isDown) {
            console.log("up");
            dude.anims.play('up', true);
        } else if (cursors.down.isDown) {
            console.log("down");
            dude.setVelocityY(300);
            dude.anims.play('down', true);
        } else if (cursors.left.isDown) {
            console.log("left");
            dude.setVelocityX(-300);
            dude.anims.play('left', true);
        } else if (cursors.right.isDown) {
            console.log("right");
            dude.setVelocityX(300);
            dude.anims.play('right', true);
        } else if (cursors.space.isDown) {
            console.log("space");
            dude.anims.play('up', true);
        } else if (cursors.shift.isDown) {
            console.log("shift");
        } else {
            dude.setVelocityX(0);
        }

        if (cursors.up.isDown && dude.body.touching.down) {
            dude.setVelocityY(-330);
        }

    }

    function collectStar(dude, star) {



        star.disableBody(true, true);
        if (stars.countActive(true) === 0) {
            stars.children.iterate(function (child) {
                child.enableBody(true, child.x, 0, true, true);
            });

            var x = (dude.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);

            var bomb = bombs.create(x, 16, 'bomb');
            bomb.setBounce(1);
            bomb.setCollideWorldBounds(true);
            bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);
            bomb.allowGravity = false
        }
        if (block.countActive(true) === 0) {
            block.children.iterate(function (child) {
                child.enableBody(true, child.x, 0, true, true);
            });

            var x = (dude.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);

            var bomb = bombs.create(x, 16, 'bomb');
            bomb.setBounce(1);
            bomb.setCollideWorldBounds(true);
            bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);
            bomb.allowGravity = false
        }
        score += 10;
        scoreText.setText('Score: ' + score);
        $("#score").html(score);
        $("#resultat").html(score);


        $("meta[property='og\\:title']").attr("content",'Vous pouvez faire mieux ? mon score est de '+ score +' !  #swe2019');
    }

    function hitBomb(dude, bomb) {

        this.physics.pause();
        var scorePromo = 50;
        dude.setTint(0xff0000);
       
        $('#myModal').show();

        console.log(score);
        // var id = 2094899294139938;
        // var link = "http://fabienporet.fr/phaser-advergame/";
        // var picture = "images/fb.PNG";
        // var name = 'Vous pouvez faire mieux ? mon score est de '+ score +' !  #swe2019';
        // var caption= "Startup weekend de Saint-lô le 11, 12 et 13 Octobre";
        // var description= "Gagne t'as place pour le Startup Weekend de Saint-lô";
        // var redirect= "http://fabienporet.fr/phaser-advergame/"; 

        // // document.location.href="share.html"
        // // var url = "https://www.facebook.com/dialog/feed?app_id={"+id+"}&link={"+link+"}&picture={"+picture+"}&name={"+name+"}&caption={"+caption+"}&description={"+description+"}&redirect_uri={"+redirect+"}";
        // var url = "https://www.facebook.com/sharer/sharer.php?u=http%3A//fabienporet.fr/phaser-advergame/";
        // $("#fb").attr("href", url);

        //    dude.anims.play('turn');
        // alert("Bravo, voici ton score : " + score);
       
        // if (this.physics.pause()) { 
        //     var txt;
        //     var r = confirm("Souhaitez-vous rejouer ?");
        //     if (r == true) {
        //         gameOver = true;
        //         if (gameOver) {
        //             document.location.reload(true);
        //         }
        //     } else {
        //         txt = "You pressed Cancel!";
        //     }
        //     if (score > scorePromo){
        //     alert("Partage ton score sur Facebook et tente de remporter des codes de réduction pour le Startup week end de Saint-lô");           
        //         // alert("Bravo, tu as un score supérieur à " + scorePromo + " : tu gagnes un code promo pour ta place au start up week end");
        //     }
        // }
        
    }

    $( "#target" ).click(function() {
        var usr = $('#usr').val();
        var table = { user: usr, resultat: score};
        console.log(table); 
      });
});